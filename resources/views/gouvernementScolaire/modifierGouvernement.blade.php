@extends('layouts.app')


@section('content')
<div class="row ">
    <div class="col-lg-8">
    <h1>Modifier un membre</h1>

    </div>
</div>



    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card ">
                <div class="card-body ">

                    <form method="post" action="{{ url('Gouvernement/' . $gouvernement->id) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf





                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Nom:</label>
                                    <input type="text" class="form-control @error('nom') is-invalid @enderror "
                                        id="nom" placeholder="Ajouter un nom" name="nom" value="{{ $gouvernement->nom }}">
                                    @error('nom')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Prénom:</label>
                                    <input type="text" class="form-control  @error('prenom') is-invalid @enderror"
                                        id="prenom" placeholder="Ajouter un prénom" name="prenom"
                                        value="{{ $gouvernement->prenom }}">
                                    @error('prenom')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Statut:</label>
                                    <input type="text" class="form-control  @error('statut') is-invalid @enderror"
                                        id="statut" placeholder="Ajouter le statut" name="statut"
                                        value="{{ $gouvernement->statut }}">
                                    @error('statut')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Photo:</label>
                                    <input type="file" name="image" class=" @error('nom') is-invalid @enderror"
                                        value="{{ $gouvernement->image }}">
                                </div>
                            </div>
                        </div>




                       <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Modifier</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

