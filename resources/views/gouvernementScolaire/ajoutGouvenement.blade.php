@extends('layouts.app')


@section('content')
    <h1>Ajouter un membre</h1>



    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body ">
                    <form class="" action="{{ url('ajout-GS') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Nom:</label>
                                    <input type="text" class="form-control   @error('nom') is-invalid @enderror"
                                        id="nom" placeholder="Ajouter un nom" name="nom"  value="{{ old('nom') }}">
                                    @error('nom')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Prénom:</label>
                                    <input type="text" class="form-control  @error('prenom') is-invalid @enderror"
                                        id="prenom" placeholder="Ajouter un prénom" name="prenom"  value="{{ old('prenom') }}">
                                    @error('prenom')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Statut:</label>
                                    <input type="text" class="form-control  @error('statut') is-invalid @enderror"
                                        id="statut" placeholder="Ajouter le statut" name="statut"  value="{{ old('statut') }}">
                                    @error('statut')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Photo:</label>
                                    <input type="file" name="image" class=" @error('nom') is-invalid @enderror"  value="{{ old('image') }}">
                                </div>
                            </div>
                        </div>

                        @guest
                        @else
                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                        @endguest


                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Ajouter</button>

                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
