@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-11">

            <h2>Liste du gouvernement scolaire</h2>

        </div>

        <div class="col-lg-1">
            <a class="btn bg-gradient-success" href="{{ url('Gouvernement/ajouter') }}">Ajouter</a>
        </div>

    </div>



    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="table-responsive">

                    <table class="table table-striped">
                        <thead>
                            <tr class="text-center">
                                {{-- <th>No</th> --}}
                                <th>Image </th>
                                <th>Nom </th>
                                <th>Prenom</th>
                                <th>Statut</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($gouvernements as $index => $gouvernement)
                                <tr class="text-center">
                                    {{-- <td>{{ $index }}</td> --}}
                                    <td>
                                        {{-- <img src="assets/images/{{$gouvernement->image }}" class="produits w-100"> --}}
                                        <img src="{{ asset('Images/'.$gouvernement->image) }}" alt="{{ $gouvernement->image }}"
                                        class="rounded-circle" alt="homme" style="width: 40px;height!">
                                    </td>
                                    <td>{{ $gouvernement->nom }}</td>
                                    <td>{{ $gouvernement->prenom }}</td>
                                    <td>{{ $gouvernement->statut }}</td>

                                    <td>

                                        <form action="{{ url('Gouvernement/' . $gouvernement->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <a class="btn btn-info" href="{{ url('Gouvernement/' . $gouvernement->id) }}"><i
                                                    class="fa fa-eye " aria-hidden="true" style="color:white"></i></a>
                                            <a class="btn btn-primary"
                                                href="{{ url('Gouvernement/' . $gouvernement->id . '/edit') }}"><i
                                                    class="fa fa-pencil" aria-hidden="true"></i></a>

                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"
                                                    aria-hidden="true"></i></button>

                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>



                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
