@extends('layouts.app')


@section('content')
    <div class="container">
        <h1>Détail membre</h1>

        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-3">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="{{ asset('assets/images/' . $gouvernement->image) }}" class="img-fluid"
                                alt="{{ $gouvernement->image }}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">{{ $gouvernement->prenom }} {{ $gouvernement->nom }}</h5>
                                <p class="">{{ $gouvernement->statut }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
