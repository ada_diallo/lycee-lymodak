{{-- @extends('Navbar.navbar')

@section('content') --}}

    <div class="row  mt-5" style="background: #F5F5F5" >
        <h3 class="text-center   mt-5 fw-bold">NOTRE GALERIE</h3>

        <div class="col-lg-12"  data-aos="fade-up" data-aos-duration="5
                000">
            <div id="carouselExampleControls" class="carousel slide galerie pt-5 mb-5" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="cards-wrapper">
                            <div class="card">

                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid" alt="...">

                                </div>

                            </div>
                            <div class="card">
                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid" alt="...">

                                </div>

                            </div>
                            <div class="card">
                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid" alt="...">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="cards-wrapper">
                            <div class="card">
                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid" alt="...">

                                </div>

                            </div>
                            <div class="card">
                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid " alt="...">

                                </div>

                            </div>
                            <div class="card">
                                <div class="image-wrapper">
                                    <img src="../../assets/Images/studients.jpg" class="img-fluid " alt="...">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </div>
{{-- @endsection --}}
