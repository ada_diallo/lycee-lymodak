@extends('layouts.app')


@section('content')


    <h1>Modifier info</h1>


    @if ($errors->any())
        <div class="alert alert-danger">

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>

        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">

                    <form method="post" action="{{ url('info-ecole/' . $info->id) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf


                        <div class="form-group mb-3">

                            <label for="nomComplet">Nom:</label>
                            <input type="text" class="form-control" id="addrese" placeholder="Entrer Nom"
                                name="nom" value="{{ $info->nom }}">

                        </div>
                        <div class="form-group mb-3">

                            <label for="nomComplet">Adresse:</label>
                            <input type="text" class="form-control" id="addrese" placeholder="Entrer Nom"
                                name="addresse" value="{{ $info->addresse }}">

                        </div>

                        <div class="form-group mb-3">

                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="" placeholder="Entrer Email"
                                name="email" value="{{ $info->email }}">

                        </div>

                        <div class="form-group mb-3">

                            <label for="telephone">Telephone:</label>
                            <input type="number" class="form-control" id="phone" placeholder="Entrer Telephone"
                                name="phone" value="{{ $info->phone }}">

                        </div>
                        <div class="form-group mb-3">

                            <label for="horaires">jour_ouverture:</label>
                            <input type="text" class="form-control" id="" placeholder="Entrer Telephone"
                                name="jour_ouverture" value="{{ $info->jour_ouverture }}">

                        </div>

                        <div class="form-group mb-3">

                            <label for="horaires">jour_ouverture:</label>
                            <input type="text" class="form-control" id="" placeholder="Entrer Telephone"
                                name="jour_fermeture" value="{{ $info->jour_fermeture }}">

                        </div>
                        <div class="form-group mb-3">

                            <label for="horaires">jour_ouverture:</label>
                            <input type="time" class="form-control" id="" placeholder="Entrer Telephone"
                                name="heure_ouverture" value="{{ $info->heure_ouverture }}">

                        </div>
                        <div class="form-group mb-3">

                            <label for="horaires">jour_ouverture:</label>
                            <input type="time" class="form-control" id="" placeholder="Entrer Telephone"
                                name="heure_femeture" value="{{ $info->heure_femeture }}">

                        </div>
                        <div class="form-group mb-3">

                            <label for="telephone">historique</label>
                            <textarea name="historique" id="" cols="20" rows="6"
                                class="form-control   @error('historique') is-invalid @enderror">{{ $info->historique }} </textarea>
                            @error('histoique')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Photo:</label>
                                    <input type="file" name="image" class=" @error('image') is-invalid @enderror"
                                        value="{{ $info->image }}">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Modifier</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

