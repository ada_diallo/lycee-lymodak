@extends('layouts.app')


@section('content')
    <h1>Ajouter Info Ecole</h1>



    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body ">
                    <form action="{{ url('/info') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="nom">Nom établissement:</label>
                                    <input type="text" class="form-control " id="nom"
                                        placeholder="Entrez votre adresse" name="nom" value="{{ old('nom') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="nomComplet">Adresse:</label>
                                    <input type="text" class="form-control " id="addresse"
                                        placeholder="Entrez votre adresse" name="addresse" value="{{ old('addresse') }}">
                                </div>
                            </div>
                        </div>



                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" placeholder="Email"
                                        name="email" value="{{ old('email') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="telephone">Telephone:</label>
                                    <input type="number" class="form-control" id="phone" placeholder="telephone"
                                        name="phone" value="{{ old('phone') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="horaires">Jour ouverture:</label>
                                    <input type="text" class="form-control" id="" placeholder="Jour ouverture"
                                        name="jour_ouverture" value="{{ old('jour_ouverture') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="horaires">Jour fermeture:</label>
                                    <input type="text" class="form-control" id="" placeholder="Jour fermeture"
                                        name="jour_fermeture"  value="{{ old('jour_fermeture') }}" >
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="horaires">Heure ouverture:</label>
                                    <input type="time" class="form-control" id=""
                                        placeholder="Heure d'ouverture" name="heure_ouverture"  value="{{ old('heure_ouverture') }}" >
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="horaires">Heure fermeture:</label>
                                    <input type="time" class="form-control" id="" placeholder="Heure fermeture"
                                        name="heure_femeture"  value="{{ old('heure_femeture') }}" >
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3"> <label for="dateFn">Historique:</label>
                                    <textarea rows="10" cols="33" class="form-control  @error('historique') is-invalid @enderror"
                                        name="historique" id="description" placeholder="Votre message">{{ old('historique') }}</textarea>
                                    @error('historique')
                                        <div class=" invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label class="block mt-3 m-4"> Image</label>

                                    <input type="file" name="image"
                                        class="form-control @error('image') is-invalid @enderror "   value="{{ old('image') }}"/>
                                    @error('image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @guest
                        @else
                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                        @endguest
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Ajouter</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
