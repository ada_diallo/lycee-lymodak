@extends('layouts.app')

@section('content')
    <div class="row">

        <div class="col-lg-11">

            <h2>Info ecole</h2>
        </div>
        <div class="col-lg-1">
            <a class="bg-gradient-success p-2 "
                style="color:white;text-decoration:none; border-radius:10% ;font-weight:bolder"
                href="{{ url('/ajout-info') }}">Ajouter</a>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped table-sm ">
                        <thead>
                            <tr class="text-center">
                                {{-- <th>No</th> --}}
                                <th>Image</th>

                                <th>Nom</th>

                                <th>Adresse</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Jour ouvert</th>
                                <th>Jour fermé</th>
                                <th>Heure ouvert</th>
                                <th>Heure fermé</th>
                                <th>Historique</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($infoEcole as $index => $info)
                                <tr class="text-center">
                                    {{-- <td>{{ $index }}</td> --}}
                                    <td>
                                        <img src="{{ asset('Images/'.$info->image) }}" alt="{{ $info->image }}"
                                        alt="homme" style="width: 40px;height!"></td>
                                    <td>{{ $info->nom }}</td>

                                    <td>{{ $info->addresse }}</td>
                                    <td>{{ $info->email }}</td>
                                    <td>{{ $info->phone }}</td>
                                    <td>{{ $info->jour_ouverture }}</td>
                                    <td>{{ $info->jour_fermeture }}</td>
                                    <td>{{ $info->heure_ouverture }}</td>
                                    <td>{{ $info->heure_femeture }}</td>

                                    <td>{{ Str::limit($info->historique, 20) }}</td>


                                    <td>
                                        <form action="{{ url('info-ecole/' . $info->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <a class="btn btn-info" href="{{ url('info-ecole/' . $info->id) }}"><i
                                                    class="fa fa-eye " aria-hidden="true" style="color:white"></i></a>
                                            <a class="btn btn-primary"
                                                href="{{ url('info-ecole/' . $info->id . '/edit') }}"><i
                                                    class="fa fa-pencil" aria-hidden="true"></i></a>

                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"
                                                    aria-hidden="true"></i></button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
