@extends('layouts.app')





@section('content')
    <div class="container">
        <h1>Détail de l'établissement</h1>

        <div class="row">
            <div class="col-lg-12">
                <div class="card mb-3">
                    <div class="row">
                       
                        <div class="col-md-8">
                            <div class="card-body">
                                <div class="d-flex">
                                    <h5 class="card-title"> 
                                        {{ $info->nom }}</h5>

                                </div>
                                <p>Adresse: {{ $info->addresse }}</p>
                                <p>Email: {{ $info->email }}</p>
                                <p>Téléphone: {{ $info->phone }}</p>
                                <p> Jour ouvert: {{ $info->jour_ouverture }}</p>
                                <p>Jour fermé: {{ $info->jour_fermeture }}</p>
                                <p>Heure d'ouverture: {{ $info->heure_ouverture }}</p>
                                <p>Heure ferméture: {{ $info->heure_femeture }}</p>
                                <p>Historique: {{ $info->historique }}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>




    </div>
@endsection
