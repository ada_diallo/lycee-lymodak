<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../register.css">
    <title>Document</title>
</head>
<body>
<div class="container-fluid  mydiv">
    <div class="row justify-content-center divlog">
        <div class="col-md-6">
            <div class="card bg-light shadow  justify-content-center carte-form2" style="border:0">
                <!-- <div class="card-header">{{ __('Register') }}</div> -->

                <div class="card-body justify-content-center cartelog2">
                <h3 class="titre3 mb-4" style="text-align:center">Inscription</h3>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-7 justify-content-center">
                            <label for="name" class="col-md-2 col-form-label text-md-end">{{ __('Nom') }}</label>

                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror myinput" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-7">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Adresse email') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror myinput" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-7">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Mot de passe') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror myinput" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-7">
                            <label for="password-confirm" class="col-lg-8 col-form-label text-md-end">{{ __('Confirme mot de passe') }}</label>
                                <input id="password-confirm" type="password" class="form-control myinput" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-7">
                            <div class="d-grid gap-2">
                               <button class="btn btn-success btn-login" type="submit">{{ __("s'inscrire") }}</button>
                            </div>
                            </div>
                        </div>
                        <div class="row mb-0">
                          <div class="col-md-8 offset-md-3">
                           @if (Route::has('login'))
                                si vous avez déjà un compte <a class="btn btn-link" href="{{ route('login') }}">
                                      {{ __("connectez-vous ") }}
                                </a>
                            @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>




