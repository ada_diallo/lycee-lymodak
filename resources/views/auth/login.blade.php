<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../Login.css">
</head>
<body>
<div class="container-fluid mydiv">
    <div class="row justify-content-center  divlog">
        <!-- <div class="col-md-7  vh-100 myimage">

        </div> -->
        <div class="col-md-6 colone2 justify-content-center">
            <div class="card bg-light shadow  justify-content-center carte-form" style="border:0">
                <!-- <div class="card-header">{{ __('Login') }}</div> -->

                <div class="card-body justify-content-center cartelog">
                    <h3 class="titre1 mb-4" style="text-align:center">Connexion</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6 justify-content-center">
                            <label for="email" class="col-md-5 col-form-label text-md-end">{{ __(' Addresse Email ') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror myinput" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3 justify-content-center">

                            <div class="col-md-6">
                                <label for="email" class="col-md-5 col-form-label text-md-end">{{ __(' Mot de passe ') }}</label>
                                <input id="password" type="password" class="form-control  @error('password') is-invalid @enderror myinput" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <div class="col-md-8 offset-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label myremember" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                    <label>
                                   @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Mot de passe oublé ?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row mb-3 justify-content-center">
                            <div class="col-md-6">
                               <div class="d-grid gap-2">
                               <button class="btn btn-success btn-login" type="submit">{{ __('Se conecter') }}</button>
                            </div>
                                <!-- <button type="submit" class="btn btn-primary btn-login">
                                    {{ __('Se conecter') }}
                                </button> -->

                            </div>
                        </div>
                         <div class="row mb-0">
                          <div class="col-md-8 offset-md-3">
                           @if (Route::has('register'))
                                <!-- <li class="nav-item"> -->
                                si vous n'avez pas encore de compte <a class="btn btn-link" href="{{ route('register') }}">
                                      {{ __("s'inscrire ") }}
                                </a>
                                  <!-- <p class="col-md-8 col-form-label text-md-end">Sin ous v'avez pas de compte<a class="nav-link" href="{{ route('register') }}">{{ __("m'inscrire") }}</a></p>
                                </li> -->
                            @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
