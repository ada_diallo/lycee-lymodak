@extends('Navbar.navbar')
@section('content')
    <div class="mt-5" style="overflow: hidden">
        <div class="row mb-5 justify-content-center ">
            <div class="col-md-12 fistdiv">
                <h1 class='textDon'>Accueil/Don </h1>


            </div>
           
        </div>
        <div class="container">
            <div class="row ">
                <div class="col-lg-5">
                    <h2>Vous souhaitez nous soutenir ?</h2>
                    <p class="para">Nous souhaitons améliorer les conditions d'apprentissage et d'enseignement pour les
                        élèves de notre lycée, en leur offrant des équipements modernes et en finançant des activités et des
                        projets éducatifs.

                        Cependant, pour réaliser ces projets, nous avons besoin de votre aide. Nous vous sollicitons donc
                        pour des dons, quel que soit le montant que vous pouvez donner. Chaque contribution compte et peut
                        faire une différence dans la vie des élèves de notre lycée.
                        Si vous avez des questions ou si vous souhaitez en savoir plus sur notre projet, n'hésitez pas à
                        nous contacter.</p>
                </div>
                <div class="col-lg-7">
                    <div class='card'>
                        <div class="card-img">
                            <img src="../../assets/Images/donation.jpg" class=" img-fluid" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<div class="container"> <div class="row  justify-content-center ">
            <div class="col-lg-4 mb-5">
                <div class="card contenu">
                    <div class="card-img">
                        <img src="../../assets/Images/wave.jpg" class="img-fluid" />
                    </div>
                    <div class="overlay">
                        <div class="text">
                            <h4>Wave</h4>
                            <p>Pour vos transfère, il
                                faut envoyer par code sur ce numéro: <br />781452667</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-5">
                <div class="card contenu">
                    <div class="card-img">
                        <img src="../../assets/Images/orange.png" class="operateur img-fluid" />

                    </div>
                    <div class="overlay">
                        <div class="text">
                            <h4>Orange Money</h4>
                            <p>Pour vos transfère, il
                                faut envoyer par code sur ce numéro:<br />77 4 14 60 35</p>
                        </div>
                    </div>
                </div>
            </div>
        </div></div>
       

    </div>
@endsection

