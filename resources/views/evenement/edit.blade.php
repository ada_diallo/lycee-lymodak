@extends('layouts.app')


@section('content')
    <div class="container ">
        <h1>Modifier un évenement</h1>


        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="{{ url('evenement/' . $evenement->id) }}"
                            enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf


                            <div class="form-group mb-3">

                                <label for="nom">Nom:</label>
                                <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom"
                                    placeholder="Entrer Nom" name="nom" value="{{ $evenement->nom }}"
                                    >
                                @error('nom')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror

                            </div>

                            <div class="form-group mb-3">
                                <label for="dateDb">Date Debut:</label>
                                <input type="date" class="form-control  @error('dateDb') is-invalid @enderror"
                                    id="dateDb" placeholder="Date Debut" name="dateDb" value="{{ $evenement->dateDb }}"
                                    >
                                @error('dateDb')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group mb-3">

                                <label for="dateFn">Date Fin:</label>
                                <input type="date" class="form-control   @error('dateFn') is-invalid @enderror"
                                    id="dateFn" placeholder="Date Fin" name="dateFn" value="{{ $evenement->dateFn }}"
                                    >
                                @error('dateFn')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror

                            </div>

                            <div class="form-group mb-3">

                                <label for="telephone">Description</label>
                                <textarea name="description" id="" cols="20" rows="10"
                                    class="form-control   @error('description') is-invalid @enderror" >{{ $evenement->description }}</textarea>
                                @error('description')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="icon" class="">Image:</label>
                                <input type="file" class="form-control  @error('image') is-invalid @enderror"
                                    id="image" placeholder="Entrez une image" name="image"
                                    value="{{ $evenement->image }}" >
                                @error('image')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
 <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Modifier</button>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

