@extends('layouts.app')

@section('content')
    <div class="row ">
        <div class="col-lg-11">
            <h2>Evenement</h2>
        </div>

        <div class="col-lg-1">
            <a class="btn bg-gradient-success" href="{{ url('evenement/create') }}">Ajouter</a>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>
                            <tr class="text-center">
                                {{-- <th>No</th> --}}
                                <th>Image</th>
                                <th>Nom </th>
                                <th>Date Debut</th>
                                <th>Date Fin</th>
                                <th>Description</th>

                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($evenements as $index => $evenement)
                                <tr class="text-center">
                                    <td>
                                        <img src="{{ asset('Images/'.$evenement->image) }}" alt="{{ $evenement->image }}"
                                        style="width: 40px;height:40px">
                                    </td>
                                    <td>{{ $evenement->nom }}</td>
                                    <td>{{ $evenement->dateDb }}</td>
                                    <td>{{ $evenement->dateFn }}</td>
                                    <td>{{ Str::limit($evenement->description, 20) }}</td>
                                    <td>
                                        <form action="{{ url('evenement/' . $evenement->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <a class="btn btn-info" href="{{ url('evenement/' . $evenement->id) }}"><i
                                                    class="fa fa-eye " aria-hidden="true" style="color:white"></i></a>
                                            <a class="btn btn-primary"
                                                href="{{ url('evenement/' . $evenement->id . '/edit') }}"><i
                                                    class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"
                                                    aria-hidden="true"></i></button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
