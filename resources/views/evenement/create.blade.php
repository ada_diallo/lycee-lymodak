@extends('layouts.app')


@section('content')
    <h1>Ajouter un evenement</h1>



    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body ">

                    <form action="{{ url('evenement') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nom">Nom :</label>
                                    <input type="text" class="form-control   @error('nom') is-invalid @enderror "
                                        value="{{ old('nom') }}" id="nom" placeholder="Entrez un nom d'evenement"
                                        name="nom">
                                    @error('nom')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="dateDb">Date Debut:</label>
                                    <input type="date" class="form-control    @error('dateDb') is-invalid @enderror"
                                        id="dateDb" placeholder="date Debut" name="dateDb" value="{{ old('dateDb') }}">
                                    @error('dateDb')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="dateFn">Date Fin:</label>
                                    <input type="date" class="form-control    @error('dateFn') is-invalid @enderror"
                                        id="dateFn" placeholder="Date Fin" name="dateFn" value="{{ old('dateFn') }}">
                                    @error('dateFn')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="dateFn">Description:</label>
                                    <textarea rows="10" cols="33" class="form-control  @error('description') is-invalid @enderror"
                                        name="description" id="description" placeholder="Votre message">{{ old('description') }}</textarea>
                                    @error('description')
                                        <div class=" invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label class="block mt-3 m-4"> Image</label>

                                    <input type="file" name="image"
                                        class="    @error('image') is-invalid @enderror block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100" />
                                    @error('image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @guest
                        @else
                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                        @endguest
                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Ajouter</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
