@section('content')

    <h1>Detail Evenement</h1>

    <table class="table table-bordered">
        <tr>
            <th>Image</th>
            <td>              
                <img src="assets/img/{{ $evenement->image }}" alt="{{ $evenement->nom }}" class="evenements w-100"></td>
            </tr>
        </tr>

        <tr>
            <th>Nom:</th>
            <td>{{ $evenement->nom }}</td>
        </tr>

        <tr>
            <th>Date Debut:</th>
            <td>{{ $evenement->dateDb }}</td>
        </tr>

        <tr>
            <th>Date Fin:</th>
            <td>{{ $evenement->dateFn }}</td>
        </tr>

        <tr>
            <th>Description:</th>
            <td>{{ $evenement->description }}</td>
        </tr>

    </table>

@endsection
