@extends('layouts.app')


@section('content')
    <div class="container">
        <h1>Détail de l'évenement</h1>

        <div class="row ">
            <div class="col-lg-12">
                <div class="card mb-3">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="{{ asset('assets/images/' . $evenement->image) }}" class="img-fluid"
                                alt="{{ $evenement->image }}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Nom de l'évenement: {{ $evenement->nom }}</h5>
                                <p class="fw-bold">Date de début: {{ $evenement->dateDb }}</p>
                                <p class="">Date Fin: {{ $evenement->dateFn }}</p>
                                <p class="">Description: {{ $evenement->description }}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>




    </div>
@endsection
