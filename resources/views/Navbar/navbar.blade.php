<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- ------------------------LINK BOOTSTRAP------------------------- --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="../../css/css.css" rel="stylesheet">
    <link href="../../Login.css" rel="stylesheet">


    <link href="https://fonts.cdnfonts.com/css/montserrat" rel="stylesheet">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <title>siteEcole</title>
</head>

<body>
    {{--
    <nav class="navbar navbar-expand-lg bg-white fixed-top shadow navbar-navbar" style="height:5em">
        <div class="container-fluid">
            <h4 class="navbar-brand">LYCEE LAMINE GUEYE</h4>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end " id="navbarSupportedContent">

                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/landing_page">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#mot_DG">Mot du directeur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#historique">Historique</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#gouvernement">Gouvernement Scolaire</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#evenement">Evénements</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#footer">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                    </li>



                </ul>

                <div class="buton-contact">
                    <button class="btn " type="submit"><a href='/donation' class="text-decoration-none contact">Faire
                            un don</a></button>
                </div>
            </div>
        </div>
    </nav> --}}



    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-white py-3 shadow ">
        <div class="container-fluid">

            <a class="navbar-brand fs-4" href="#">LYCEE LAMINE GUEYE</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end " id="navbarSupportedContent">
                <ul class="navbar-nav mb-2 mb-lg-0 align-items-center">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/#">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#mot_DG">Mot du directeur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#historique">Historique</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink"
                            role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Scolarite
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                            <li><a class="dropdown-item" href="/etablissement">Etablissements</a></li>
                            <li><a class="dropdown-item" href="/condition-acces">Condition d'Acces</a></li>
                            <li><a class="dropdown-item" href="/reglement-interieur">Reglement Interieur</a></li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#gouvernement">Gouvernement Scolaire</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#evenement">Evénements</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#footer">Contact</a>
                    </li>
                    <div class="buton-contact ">
                        <button class="btn " type="submit"><a href='/donation'
                                class="text-decoration-none contact">Faire
                                un don</a></button>
                    </div>

                </ul>

            </div>
        </div>
    </nav>


    <div class="">
        @yield('content')

    </div>


    <div class=" " style="background:#009688;color:#FFFF" id="footer">
        <div class="container">
            <div class="row d-flex align-items-center">
                <h3 class="text-center mt-5 fw-bold">NOUS CONTACTER</h3>
                <div class="col-lg-6 mt-5">
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    <form class="" action="{{ url('nous-contacter') }}" method="POST">
                        @csrf
                        <div class="row ">
                            <div class="col-sm-6 col-xs-12 mb-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" placeholder="Nom complet*"
                                        name="nom_complet">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 mb-3">
                                <div class="form-group"><input type="email" class="form-control" id="email"
                                        placeholder="Email*" name="email" value=""></div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-12">
                                <div class="form-group"><input type="text" class="form-control" id="subject"
                                        placeholder="Objet" name="objet" value=""></div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-12 ">
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-12 ">
                                <div class="single-contact-btn">
                                    <div class="buton-contact ">
                                        <button class="btn " type="submit">Envoyer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @guest
                    <div class="col-lg-6 d-flex flex-row justify-content-center ">
                        <div class="single-contact-box ">
                            <div class="contact-adress">
                                <div class="contact-add-head">
                                    <h5>
                                        <x-fas-address-card style="width: 18px;height:18px" /> Adresse
                                    </h5>
                                    <p>Hlm grand yoff</p>
                                </div>
                                <div class="contact-add-info">
                                    <div class="single-contact-add-info">
                                        <h5>
                                            <x-entypo-phone style="width: 18px;height:18px" /> phone
                                        </h5>
                                        <p>78-145-26-67</p>
                                    </div>
                                    <div class="single-contact-add-info">
                                        <h5>
                                            <x-fontisto-email style="width: 18px;height:18px" /> email
                                        </h5>
                                        <p>adadialo2203@gmail.com</p>
                                    </div>
                                    <div class="single-contact-add-info">
                                        <h5>
                                            <x-uni-schedule style="width: 18px;height:18px" /><span
                                                style="padding-left:5px">Horaires</span>
                                        </h5>
                                        <p>lun-sam/08:00-18h00</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                @else
                    <div class=" col-lg-6 d-flex justify-content-start justify-content-center">
                        <div class="single-contact-box ">
                                <div class="contact-adress">
                                    <div class="contact-add-head">
                                        <h5>
                                            <x-fas-address-card style="width: 18px;height:18px" /> Adresse
                                        </h5>
                                        @foreach ($infoEcole as $index => $info)

                                        <p class="">{{ $info->addresse }}</p>
                                        @endforeach

                                    </div>
                                    <div class="contact-add-info">
                                        <div class="single-contact-add-info">
                                            <h5>
                                                <x-entypo-phone style="width: 18px;height:18px" /> Téléphone
                                            </h5>
                                            @foreach ($infoEcole as $index => $info)

                                            <p>{{ $info->phone }}</p>
                                            @endforeach
                                        </div>
                                        <div class="single-contact-add-info">
                                            <h5>
                                                <x-fontisto-email style="width: 18px;height:18px" /> email
                                            </h5>
                                            @foreach ($infoEcole as $index => $info)

                                            <p>{{ $info->email }}</p>
                                            @endforeach
                                        </div>
                                        <div class="single-contact-add-info">
                                            <h5>
                                                <x-uni-schedule style="width: 18px;height:18px" /><span
                                                    style="padding-left:5px">Horaires</span>
                                            </h5>
                                            @foreach ($infoEcole as $index => $info)

                                            <p>{{ $info->jour_ouverture }}-{{ $info->jour_fermeture }}/{{ $info->heure_ouverture }}-{{ $info->heure_femeture }}
                                            </p>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

            @endguest
        </div>

    </div>
    <div class="mt-3  bg-white text-center align-items-center" style="padding: 12px;color:#000">
        <p class="fs-6 ">Copyright ©2023 - Réalisé par Bakeli School of Technology.</p>
    </div>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

</body>

</html>
