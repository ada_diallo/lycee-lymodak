@extends('Navbar.navbar')

@section('content')

        <div class="landing-page" style="margin-top: 80px">
            {{-- ------------------------------------HEADER CAROUSSEL------------------------ --}}
            <div class="header">
                <div id="carouselExampleControls" class="carousel slide " data-bs-ride="carousel">
                    <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="carousel-caption d-none d-md-block">
                                    @foreach ($infoEcole as $index => $info)

                                    <h1 class="fw-bold ">Bienvenue au Lycee {{ $info->nom }}</h1>
                                    @endforeach
                                </div>
                                <img src="../../assets/Images/ecole.png" class="d-block w-100" alt="...">

                            </div>
                            <div class="carousel-item ">
                                <div class="carousel-caption d-none d-md-block">
                                    @foreach ($infoEcole as $index => $info)

                                    <h1 class="fw-bold ">Bienvenue au Lycee {{ $info->nom }}</h1>
                                    @endforeach                                </div>
                                <img src="../../assets/Images/carous.png" class="d-block w-100" alt="...">

                            </div>

                            <div class="carousel-item ">
                                <div class="carousel-caption d-none d-md-block">
                                    @foreach ($infoEcole as $index => $info)

                                    <h1 class="fw-bold ">Bienvenue au Lycee {{ $info->nom }}</h1>
                                    @endforeach                                </div>
                                <img src="../../assets/Images/ecole.png" class="d-block w-100" alt="...">

                            </div>
                    </div>
                </div>
            </div>

            {{-- ----------------------------------HISTORIQUE------------------------------------------- --}}
            <div class="" style="background: #009688;color:#FFFF" id='historique'>
                <div class="container">
                    <div class="row  " data-aos="fade-up" data-aos-duration="2
                000">
                        <h2 class="mt-5 text-center fw-bold">HISTORIQUE</h2>
                        <div class="col-lg-12 justify-content-center align-items-center mt-3">
                            @foreach ($infoEcole as $index => $info)
                                <p class="text-justify fs-5 mb-5 ">
                                    {{ $info->historique }}

                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
            {{-- ---------------------------------MOT DU PROVISEUR-------------------- --}}
            <div class="container  contain-dg mb-5" id="mot_DG">
                <div class="row mt-5" data-aos="fade-up" data-aos-duration="2000">
                    <h3 class="text-center mb-5 fw-bold">MOT DU PROVISEUR</h3>
                    @foreach ($directeurs as $index => $directeur)
                        <div class="col-lg-6">
                            <div class="card border-0 shadow">
                                <img src="{{ asset('Images/'.$directeur->image) }}" alt="{{ $directeur->name }}"
                                alt="{{ $directeur->image }}" class="img-fluid">
                                <div class="card-footer">
                                    <h5 class="text-center fw-bold">
                                        {{ $directeur->nomComplet }}
                                    </h5>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">

                            <p class="fs-5">
                                {{ $directeur->motDirecteur }}
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
            {{-- ---------------------------------------------GOUVERNEMENT SCOLAIRE----------------------------------------------------- --}}

            <div style="background:#F9F9F9; " id="gouvernement">
                <div class="container justify-content-center align-items-center " data-aos="fade-up"
                    data-aos-duration="2000">

                    <section class="">
                        <div class="row d-flex justify-content-center">
                            <div class=" text-center">
                                <h3 class="text-center mt-5  pb-4 fw-bold">GOUVERNEMENT SCOLAIRE</h3>

                            </div>
                        </div>

                        <div class="row text-center  ">
                            @foreach ($gouvernements as $index => $gouvernement)
                                <div class="col-lg-3 mb-5 mb-md-5 ">
                                    <div class="card gouvernement-card">
                                        <div class="avatar mx-auto bg-white">

                                            <img src="{{ asset('Images/'.$gouvernement->image) }}" alt="{{ $gouvernement->image }}"
                                            class="rounded-circle img-fluid" alt="{{ $gouvernement->image }}">
                                        </div>
                                        <div class="card-body">
                                            <h4 class="">{{ $gouvernement->prenom }} {{ $gouvernement->nom }}</h4>
                                            <hr />
                                            <p class="dark-grey-text ">
                                                {{ $gouvernement->statut }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </section>


                </div>
            </div>
            {{-- -----------------------------------------EVENEMENTS---------------------------------------------- --}}
            <div class="" id='evenement'>
                <div>
                    @include('evenement')

                </div>

            </div>
    </div>
@endsection
