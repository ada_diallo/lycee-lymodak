
    <div class="" style="background:#F9F9F9;">
        <div class="container pb-5">
            <div class="row" data-aos="fade-up" data-aos-duration="2000">
                <h3 class="text-center mt-5  pb-4 fw-bold">EVENEMENTS</h3>
                @foreach ($evenements as $evenement)
                    <div class="col-lg-4 ">
                        <div class="card evenement-card ">
                            <img src="{{ asset('Images/'.$evenement->image) }}" alt="{{ $evenement->image }}"
                            class="produits w-100">
                            {{-- <img src="{{ asset('storage/images/' . $evenement->image) }}" class="img-fluid"> --}}
                            <div class="card-body">
                                <h4 class="card-title">{{ $evenement->nom }}</h4>
                                <p style=" font-weight:bolder; ">{{ $evenement->dateDb }} {{ $evenement->dateDb }}</p>
                                <p class="card-text">
                                    {{ Str::limit($evenement->description, 10) }}
                                </p>
                                <div class="mb-3">
                                    <form action="{{ url('/evenement' . $evenement->id) }}" method="POST">
                                        @csrf

                                        <div class="voir-plus">
                                            <button class="btn"> <a class=""
                                                    href="{{ url('/evenement' . $evenement->id) }}">Voir
                                                    plus</a>
                                            </button>
                                        </div>

                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach

            </div>


        </div>
    </div>
    </div>

