@extends('layouts.app')

@section('content')
    <h1>Modifier </h1>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ url('directeur/' . $directeurs->id) }}" enctype="multipart/form-data"
                        class="justify-content-center">
                        @method('PATCH')
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="nomComplet">Nom Complet:</label>
                                    <input type="text" class="form-control  @error('nomComplet') is-invalid @enderror "
                                        id="nomComplet" placeholder="Entrer Nom" name="nomComplet"
                                        value="{{ $directeurs->nomComplet }}">
                                    @error('nomComplet')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="telephone">Mot du Directeur:</label>
                                    <textarea name="motDirecteur" id="" cols="20" rows="10"
                                        class="form-control  @error('motDirecteur') is-invalid @enderror">
                                        {{ $directeurs->motDirecteur }}</textarea>
                                    @error('motDirecteur')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="icon">Image:</label>
                                    <input type="file" class="form-control @error('image') is-invalid @enderror"
                                        id="image" placeholder="Entrez une image" name="image"
                                        value="{{ $directeurs->image }}">
                                    @error('image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Modifier</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

