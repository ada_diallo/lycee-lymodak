@extends('layouts.app')


@section('content')
    <div class="container">
        <h1>Détail directeur</h1>

        <div class="row ">
            <div class="col-lg-12">
                <div class="card mb-3">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="{{ asset('assets/images/' . $directeurs->image) }}" class="img-fluid"
                                alt="{{ $directeurs->image }}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">{{ $directeurs->nomComplet }}</h5>
                                <p class="card-text">{{ $directeurs->motDirecteur }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{-- <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Image</th>

                            <td>
                                <img src=" asset/images{{ $directeurs->image }}" class="rounded-circle"
                                    alt="homme" style="width: 40px;height!">
                            </td>
                        </tr>
    </div> --}}
    </div>
@endsection
