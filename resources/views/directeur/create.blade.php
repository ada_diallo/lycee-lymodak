@extends('layouts.app')


@section('content')
    <h1>Ajouter un membre</h1>



    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body ">
                    <form action="{{ url('directeur') }}" method="POST" enctype="multipart/form-data">
                        @csrf


                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label class="mt-3" for="nomComplet">Nom Complet:</label>
                                    <input type="text" class="form-control  @error('nomComplet') is-invalid @enderror "
                                        id="nomComplet" placeholder="Entrez un votre nom complet" name="nomComplet"  value="{{ old('nomComplet') }}">
                                    @error('nomComplet')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label for="telephone">Mot du Directeur</label>
                                    <textarea class="form-control @error('motDirecteur') is-invalid @enderror" name="motDirecteur"
                                        placeholder="Mot du directeur" rows="8" cols="33" id="motDirecteur" >  {{ old('heure_femeture') }}
     </textarea>
                                    @error('motDirecteur')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror



                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="form-group mb-3">
                                    <label class="block "> Image</label>

                                    <input type="file" name="image"  value="{{ old('image') }}"
                                        class="block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100    @error('image') is-invalid @enderror" />
                                    @error('image')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        @guest
                        @else
                            <input id="user_id" type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                        @endguest

                        <div class="row justify-content-center mb-3">
                            <div class="col-lg-4">
                                <button type="submit" class="btn"
                                    style="background: #5AB15E;color:#ffff">Ajouter</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
