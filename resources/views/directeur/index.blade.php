 @extends('layouts.app')

 @section('content')
     <div class="container">
         <div class="row">

             <div class="col-lg-11">

                 <h2>Liste des directeurs</h2>

             </div>

             <div class="col-lg-1">
                 <a class="btn bg-gradient-success" href="{{ url('directeur/create') }}">Ajouter</a>
             </div>

         </div>

         <div class="row justify-content-center">
             <div class="col-lg-12">
                 <div class="card">
                     <div class="table-responsive">
                         <table class="table table-striped ">
                             <thead>
                                 <tr class="text-center">
                                     <th>Image</th>
                                     <th>Nom Complet</th>
                                     <th>Mot du Directeur</th>
                                     <th>Actions</th>
                                 </tr>
                             </thead>
                             <tbody>

                                 @foreach ($directeurs as $index => $directeur)
                                     <tr class="text-center">
                                         <td>
                                             {{-- <img src="assets/images/{{  $directeur->image }}" class="produits w-100"> --}}
                                             <img src="{{ asset('Images/'.$directeur->image) }}" alt="{{ $directeur->image }}"
                                             class="rounded-circle" alt="homme" style="width: 40px;height:40px">
                                         </td>
                                         <td>{{ $directeur->nomComplet }}</td>
                                         <td>{{ Str::limit($directeur->motDirecteur, 20) }}</td>

                                         <td>
                                             <form action="{{ url('directeur/' . $directeur->id) }}" method="POST">
                                                 @csrf
                                                 @method('DELETE')

                                                 <a class="btn btn-info" href="{{ url('directeur/' . $directeur->id) }}"><i
                                                         class="fa fa-eye " aria-hidden="true" style="color:white"></i></a>
                                                 <a class="btn btn-primary"
                                                     href="{{ url('directeur/' . $directeur->id . '/edit') }}"><i
                                                         class="fa fa-pencil" aria-hidden="true"></i></a>

                                                 <button type="submit" class="btn btn-danger"><i class="fa fa-trash"
                                                         aria-hidden="true"></i></button>
                                             </form>
                                         </td>

                                     </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     @endsection
