
    <div class="results" style="height:80vh;">


        <div class="container contain-result" data-aos="fade-up" data-aos-duration="2000">
            <h3 class="text-center  pb-4 fw-bold">NOS RESULTATS</h3>

            <div class="row ">

                <div class="col-lg-4  mb-md-5 ">
                    <div class="card ">
                        <div class="card-body">
                            <h5 class="">2018-2019</h5>
                            <p class="card-text">
                               67%
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4  mb-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">2019-2020</h5>
                            <p class="card-text">
                               57%
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4  mb-md-5 ">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">2010-2021</h5>
                            <p class="card-text">
                               87%
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
{{-- @endsection --}}
