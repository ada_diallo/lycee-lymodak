@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead>

                            <tr class="text-center">
                                <th>No</th>
                                <th>Nom complet</th>
                                <th>Email</th>
                                <th>Objet</th>
                                <th>Message</th>




                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($listeContact as $index => $contact)
                                <tr class="text-center">


                                    <td>{{ $index }}</td>
                                    <td>{{ $contact->nom_complet }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->objet }}</td>
                                         <td>{{ Str::limit($contact->message, 20) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
