<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gouvernementScolaire extends Model
{
    use HasFactory;
    protected $fillable = [
        'nom',
        'prenom',
        'image',
        'statut',
        'user_id'





    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
