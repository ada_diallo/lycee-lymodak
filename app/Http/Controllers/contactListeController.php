<?php

namespace App\Http\Controllers;

use App\Models\contactModel;
use Illuminate\Http\Request;

class contactListeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listeContact=contactModel::all();
        return view('nousContacter',compact('listeContact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nom_complet' => 'bail|required|between:2,20|alpha',
            'objet' => 'bail|required|between:3,20|alpha',
            'message' => 'bail|required|max:250',


            // 'image' => 'required|file|mimes|:jpeg,png,jpg,gif,svg',



        ]);



        $contacts = new contactModel([

            'nom_complet' => $request->get('nom_complet'),
            'objet' => $request->get('objet'),
            'email' => $request->get('email'),

            'message' => $request->get('message'),



        ]);

        $contacts->save();
        // dd($contacts);

        return redirect('/')->with('success', 'Message envoyé avec succés');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
