<?php

namespace App\Http\Controllers;

use App\Models\Directeur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DirecteurController extends Controller
{
    public function index()
    {
        $user = Auth::id();

        $directeurs = Directeur::where('user_id', $user)->get();

        return view('directeur.index', compact('directeurs'));
    }

    public function create()
    {

        return view('directeur.create');
    }

    /**
     * Enregistre un nouveau Directeur dans la base de données
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomComplet' => 'regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            // 'motDirecteur' => 'regex:/^[a-zA-Z\s]*$/|max:550',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);

        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $imageName);

        $directeurs = new Directeur([
            'nomComplet' => $request->get('nomComplet'),
            'motDirecteur' => $request->get('motDirecteur'),
            'user_id' => $request->get('user_id'),

            'image' => $imageName,

        ]);

        $directeurs->save();
        return redirect('/liste-directeur')->with('success', 'Directeur Ajouté avec succès');
    }

    /**
     * Affiche les détails d'un Directeur spécifique
     */

    public function show($id)
    {
        $directeurs = Directeur::findOrFail($id);
        return view('directeur.show', compact('directeurs'));
    }

    /**
     *  formulaire de modification
     */

    public function edit($id)
    {
        $directeurs = Directeur::findOrFail($id);
        return view('directeur.edit', compact('directeurs'));
    }

    /**
     * Enregistre la modification dans la base de données
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'nomComplet' => 'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'motDirecteur' => 'required|regex:/^[a-zA-Z\s]*$/|max:550',
            'image' => 'file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);

        $directeurs = Directeur::findOrFail($id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->storeAs('public/images', $imageName);
            $directeurs->image = $imageName;
        }

        $directeurs->nomComplet = $request->get('nomComplet');
        $directeurs->motDirecteur = $request->get('motDirecteur');

        $directeurs->update();
        return redirect('/liste-directeur')->with('success', 'Directeur Modifié avec succès');
    }

    /**
     * Supprime le Directeur dans la base de données
     */
    public function destroy($id)
    {
        $directeurs = Directeur::findOrFail($id);
        $directeurs->delete();
        return redirect('/liste-directeur')->with('success', 'Directeur Supprime avec succès');
    }
}
