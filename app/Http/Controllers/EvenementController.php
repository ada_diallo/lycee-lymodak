<?php

namespace App\Http\Controllers;

use App\Models\Evenement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class EvenementController extends Controller
{
    /**
     * Affiche la liste des evenements
     */
    public function index()
    {
        $user = Auth::id();


        $evenements = Evenement::where('user_id',$user)->get();
        return view('evenement.index', compact('evenements'));
    }



    /**
     * return le formulaire de création d'un evenement
     */
    public function create()
    {

        return view('evenement.create');
    }
    /**
     * Enregistre un nouveau evenement dans la base de données
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',

            'dateDb' => 'required',
            // 'description'  => '|regex:/^[a-zA-Z\s]*$/|max:550',
            'image' =>  'file|mimes:jpg,jpeg,png,gif',
        ]);
        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $imageName);

        $contact = new Evenement([
            'nom' => $request->get('nom'),
            'dateDb' => $request->get('dateDb'),
            'dateFn' => $request->get('dateFn'),
            'description' => $request->get('description'),
            'user_id' => $request->get('user_id'),

            'image' => $imageName,
        ]);

        $contact->save();
        return redirect('/liste-evenement')->with('success', 'Evenement Ajouté avec succès');
    }

    /**
     * Affiche les détails d'un evenement spécifique
     */

    public function show($id)
    {
        $evenement = Evenement::findOrFail($id);
        return view('evenement.show', compact('evenement'));
    }

    public function detail($id)
    {
        $evenement = Evenement::findOrFail($id);
        return view('evenement.detailEven', compact('evenement'));
    }

    /**
     * Return le formulaire de modification
     */

    public function edit($id)
    {
        $evenement = Evenement::findOrFail($id);
        return view('evenement.edit', compact('evenement'));
    }

    /**
     * Enregistre la modification dans la base de données
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',

            'dateDb' => 'required',
            'image' =>  'file|mimes:jpg,jpeg,png,gif|',
        ]);

        $evenement = Evenement::findOrFail($id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->storeAs('public/images', $imageName);
            $evenement->image = $imageName;
        }
        $evenement->nom = $request->get('nom');
        $evenement->dateDb = $request->get('dateDb');
        $evenement->dateFn = $request->get('dateFn');
        $evenement->description = $request->get('description');

        $evenement->update();
        return redirect('/liste-evenement')->with('success', 'Evenement Modifié avec succès');
    }

    /**
     * Supprime le contact dans la base de données
     */
    public function destroy($id)
    {
        $evenement = Evenement::findOrFail($id);
        $evenement->delete();
        return redirect('/liste-evenement')->with('success', 'Evenement Supprime avec succès');
    }
}
