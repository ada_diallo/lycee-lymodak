<?php

namespace App\Http\Controllers;

use App\Models\Directeur;
use App\Models\Evenement;
use App\Models\gouvernementScolaire;
use App\Models\infoEcole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class pageAccueilController extends Controller
{
    public function index()
    {
        $user = Auth::id();


        $directeurs = Directeur::latest()->limit(1)->where('user_id', $user)->get();
        $infoEcole =infoEcole::latest()->limit(1)->where('user_id', $user)->get();

        $gouvernements = gouvernementScolaire::where('user_id',$user)->get();
        $evenements= Evenement::latest()->limit(4)->where('user_id',$user)->get();


        return view('LandingPage.landingPage', compact('directeurs','gouvernements','evenements','infoEcole'));
    }
    
}
