<?php

namespace App\Http\Controllers;

use App\Models\contactModel;
use App\Models\Directeur;
use App\Models\Evenement;
use App\Models\gouvernementScolaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index () {
        $user=Auth::id();

        $evenementNumber = Evenement::where('user_id',$user)->count();
        $gouvernementNumber = gouvernementScolaire::where('user_id', $user)->count();
        $directeurNumber = Directeur::where('user_id', $user)->count();
        $listeContact = contactModel::count();



        return view('dashboard', compact('evenementNumber', 'gouvernementNumber','directeurNumber','listeContact'));
    }
           

}
