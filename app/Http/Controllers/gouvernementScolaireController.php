<?php

namespace App\Http\Controllers;

use App\Models\gouvernementScolaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class gouvernementScolaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::id();

        $gouvernements = gouvernementScolaire::where('user_id',$user)->get();


        return view('gouvernementScolaire.listeGouvScolaire', compact('gouvernements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        return view('gouvernementScolaire.ajoutGouvenement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([
            'nom' => 'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'prenom' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'statut' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'image' =>  'file|mimes:jpg,jpeg,png,gif',


            // 'image' => 'required|file|mimes|:jpeg,png,jpg,gif,svg',
        ]);

        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $imageName);

        $gouvernementScolaire = new gouvernementScolaire([

            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'image' => $imageName,
            'user_id' => $request->get('user_id'),

            'statut' => $request->get('statut'),



        ]);


        $gouvernementScolaire->save();
        return redirect('/liste-membre')->with('success', 'Directeur Ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $gouvernement = gouvernementScolaire::findOrFail($id);

        return view('gouvernementScolaire.detail',  compact('gouvernement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gouvernement = gouvernementScolaire::findOrFail($id);
        return view('gouvernementScolaire.modifierGouvernement', compact('gouvernement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'prenom' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'statut' =>
            'required|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'image' =>  'file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);




        $gouvernement = gouvernementScolaire::findOrFail($id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->storeAs('public/images', $imageName);
            $gouvernement->image = $imageName;
        }
        $gouvernement->nom = $request->get('nom');
        $gouvernement->prenom = $request->get('prenom');
        $gouvernement->statut = $request->get('statut');



        $gouvernement->update();

        return redirect('/liste-membre')->with('success', 'Produit modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gouvernement = gouvernementScolaire::findOrFail($id);
        $gouvernement->delete();

        return redirect('/liste-membre')->with('success', '');
    }
}
