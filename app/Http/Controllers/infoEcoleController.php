<?php

namespace App\Http\Controllers;

use App\Models\infoEcole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class infoEcoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::id();
        $infoEcole = infoEcole::where('user_id',$user)->get();
        return view('infoEcole.listeInfo', compact('infoEcole'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        return view('infoEcole.ajoutInfo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'addresse' => 'required|string|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'nom' => 'required|string|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'historique' => 'required|max:1050',


            'email' => 'required',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:7',
            'jour_ouverture' => 'required|regex:/^[a-zA-Z\s]*$/|min:3|max:8',
            'jour_fermeture' => 'required|regex:/^[a-zA-Z\s]*$/|min:3|max:8',
            'image' =>  'file|mimes:jpg,jpeg,png,gif|',



        ]);

        $image = $request->file('image');
        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images'), $imageName);

        $infoEcole = new infoEcole([

            'addresse' => $request->get('addresse'),
            'nom' => $request->get('nom'),
            'historique' => $request->get('historique'),
            'image' => $imageName,


            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'user_id' => $request->get('user_id'),

            'jour_ouverture' => $request->get('jour_ouverture'),
            'jour_fermeture' => $request->get('jour_fermeture'),
            'heure_ouverture' => $request->get('heure_ouverture'),
            'heure_femeture' => $request->get('heure_femeture'),




        ]);


        $infoEcole->save();
        return redirect('/info-ecole')->with('success', 'Info Ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $info = infoEcole::findOrFail($id);

        return view('infoEcole.detailInfo',  compact('info'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info = infoEcole::findOrFail($id);
        return view('infoEcole.modifierInfo', compact('info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'addresse' => 'required|string|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'nom' => 'required|string|regex:/^[a-zA-Z\s]*$/|min:2|max:50',
            'historique' => 'required|max:1050',


            'email' => 'required',
            // 'phone' => 'required|regex:/^[0-9]$/|min:0|max:9,',
            'jour_ouverture' => 'required|regex:/^[a-zA-Z\s]*$/|min:3|max:8',
            'jour_fermeture' => 'required|regex:/^[a-zA-Z\s]*$/|min:3|max:8',
            'image' =>  'file|mimes:jpg,jpeg,png,gif|max:1024',
        ]);




        $info = infoEcole::findOrFail($id);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->storeAs('public/images', $imageName);
            $info->image = $imageName;
        }

        $info->addresse = $request->get('addresse');
        $info->nom = $request->get('nom');
        $info->historique = $request->get('historique');

        $info->phone = $request->get('phone');
        $info->email = $request->get('email');

        $info->jour_ouverture = $request->get('jour_ouverture');
        $info->jour_fermeture = $request->get('jour_fermeture');
        $info->heure_ouverture = $request->get('heure_ouverture');
        $info->heure_femeture = $request->get('heure_femeture');




        $info->update();

        return redirect('/info-ecole')->with('success', 'Info école modifié avec succès');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info = infoEcole::findOrFail($id);
        $info->delete();

        return redirect('/info-ecole')->with('success', 'Info école supprimé avec succés');
    }
}
