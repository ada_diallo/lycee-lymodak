
<?php

use App\Http\Controllers\contactListeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DirecteurController;
use App\Http\Controllers\EvenementController;
use App\Http\Controllers\gouvernementScolaireController;
use App\Http\Controllers\infoEcoleController;
use App\Http\Controllers\navBarController;
use App\Http\Controllers\pageAccueilController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', [pageAccueilController::class, 'index']);

Route::get('/donation', [navBarController::class, 'index']);

Route::get('/condition-acces', function () {
    return view('Scolarite.conditionAcce');
});
Auth::routes();

Route::post('/logout', function () {
    Auth::logout();
    return redirect('/login');
})->name('logout');

Route::middleware(['auth'])->group(function () {
    // Les routes nécessitant une connexion se trouvent ici
    // -------------------------DASHBOARD----------------------------
    Route::get('/dashboard', [DashboardController::class, 'index']);

    Route::controller(gouvernementScolaireController::class)->group(
        function () {
            Route::post('/ajout-GS', 'store')->name('ajout');

            Route::get('/liste-membre', 'index');
            Route::get('/Gouvernement/ajouter', 'create');
            Route::get('/Gouvernement/{id}', 'show');
            Route::get('/Gouvernement/{id}/edit', 'edit');
            Route::patch('/Gouvernement/{id}', 'update');
            Route::delete('/Gouvernement/{id}', 'destroy');
        }
    );

    // --------------------------INFO ECOLE---------------------------------
    Route::controller(infoEcoleController::class)->group(function () {

        Route::get('/info-ecole', 'index');
        Route::get('/ajout-info', 'create');
        Route::get('/info-ecole/{id}', 'show');
        Route::get('/info-ecole/{id}/edit', 'edit');

        Route::post('/info', 'store');
        Route::patch('/info-ecole/{id}', 'update');
        Route::delete('/info-ecole/{id}', 'destroy');
    });

    // FALY----------------------------------------------CRUD DIRECTEUR-----------------------------------FALY//
    Route::get('/liste-directeur', [DirecteurController::class, 'index'])->name('index');
    Route::get('/directeur/create', [DirecteurController::class, 'create'])->name('create');
    Route::get('/directeur/{id}', [DirecteurController::class, 'show'])->name('show');
    Route::get('/directeur/{id}/edit', [DirecteurController::class, 'edit'])->name('edit');
    Route::post('/directeur', [DirecteurController::class, 'store'])->name('store');
    Route::patch('/directeur/{id}', [DirecteurController::class, 'update'])->name('update');
    Route::delete('/directeur/{id}', [DirecteurController::class, 'destroy'])->name('destroy');
    // -------------------------------------------EVENEMENTS----------------------------------------
    Route::controller(EvenementController::class)->group(function () {

        Route::get('/liste-evenement', 'index');
        Route::get('/evenement/create', 'create');
        Route::get('/evenement/{id}', 'show');
        Route::get('/evenement/{id}/edit', 'edit');

        Route::post('/evenement', 'store');
        Route::patch('/evenement/{id}', 'update');
        Route::delete('/evenement/{id}', 'destroy');
    });
    Route::post('/nous-contacter', [contactListeController::class, 'store']);
    Route::get('/liste-contact', [contactListeController::class, 'index']);

});
Route::post('/nous-contacter', [contactListeController::class, 'store']);
Route::get('/liste-contact', [contactListeController::class, 'index']);
